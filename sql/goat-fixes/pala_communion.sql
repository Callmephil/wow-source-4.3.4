--
DELETE FROM spell_script_names WHERE ScriptName = 'spell_pal_communion';
DELETE FROM spell_script_names WHERE spell_id = 63531;
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
('465', 'spell_pal_communion'),
('7294', 'spell_pal_communion'),
('19746', 'spell_pal_communion'),
('19891', 'spell_pal_communion'),
('32223', 'spell_pal_communion'),
('63510', 'spell_pal_communion');